#include "Uart.h"
#include "Flash.h"

// Ptr to the our main program
typedef void (*ptrFun)(void);
ptrFun mainFunc = (ptrFun)0x6000;

void  main()
{
    uint8_t buffRxCommand[10];
    uint16_t len;
    Drivers::Flash falsh;
    Drivers::Uart uart(Drivers::Uart::kUartNumber1, Drivers::Uart::kUartBaudRate19200);
    char* command = (char*)"update";
     
    for (;;) {
        uart.sendData((uint8_t*)command, strlen(command));
        // Wait while all bytes will be sent
        while(!uart.getStatusTx()) {
        }
		
        // In this place there should be a block for receiving a package, 
        // taking into account the length and the timeout value.
        // For a test application, we simplify this procedure.

        // Read all received bytes 
        uart.readData(buffRxCommand, len, sizeof(buffRxCommand)); 
        // Compare Rx and Tx data 
        if (!memcmp(command, &buffRxCommand[0], strlen(command))) {
            // Erase memory area from 0x8000 to 0x8400 and write there values 0xAA
            if (!falsh.write(0x8000, 0xAA, 0x400)) {
                // Error write data
            }
        } else {
			// Jump to main firmware
            mainFunc();
        }
    }
}