#include "Flash.h"

/**
  * @brief  Write a single byte to temporary cell
  * @param  address: Flash relative address to write (0 ... getSize())
  * @param  value: 8-bit data
  * @retval true if the m_TmpValue is full and ready to write to flash, otherwise return false
  */
bool Drivers::Flash::tmpWrite(uint32_t address, uint8_t value)
{
    uint8_t index = (address & 0x3);
    uint8_t shiftValue[] = {24, 16, 8, 0};
	
    if (index == 0)
        m_tmpValue = 0;
	
    m_tmpValue |= ((uint32_t)(value) << shiftValue[index]);
	
    return (index == 3);
}

/**
  * @brief  Check address and if it is the begin of a sector erase it
  * @param  address: Flash relative address to write (0 ... getSize())
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::checkSector(uint32_t address)
{
    FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t PageError;

    if ((address % FLASH_PAGE_SIZE) != 0)
        return true;
	
    // Fill EraseInit structure 
    EraseInitStruct.TypeErase 	 = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress  = address;
    EraseInitStruct.NbPages		 = 1;
	
    HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	
    return (PageError == 0xFFFFFFFFU);
}

/**
  * @brief  Write a single byte to Flash
  * @param  data: 8-bit data
  * @param  address: Flash relative address to write (0 ... getSize())
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::write(uint32_t address, uint8_t data)
{
    bool status = false;

    // Relative address -> to Absolute
    address += FLASH_BASE;

    // Check the Flash real addresss
    if (!IS_FLASH_DATA_ADDRESS(address)) {
        return false;
    }

	if (checkSector(address) == false) {
		return false;
	}
		
	if (tmpWrite(address, data) == false) {
		return true;
	}
	
    // Unlock Flash (enable Write accsess)
    if (HAL_FLASH_Unlock() == HAL_OK) {
        // Program byte at a specified address
        if (HAL_FLASH_Program(TYPEPROGRAM_WORD, address - 3, m_tmpValue) == HAL_OK)  {
            status = true;
        }
        // Lock FLASH (disable Write accsess)
        if (HAL_FLASH_Lock() != HAL_OK) {
            status = false;
        }
    }
	
    return status;
}

/**
  * @brief  Read a single byte from EEPROM
  * @param  address: EEPROM relative address to read
  * @param  data: returned data link
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::read(uint32_t address, uint8_t& data)
{
    // Relative address -> to Absolute
    address += FLASH_BASE;

    // Check the EEPROM real addresss
    if (!IS_FLASH_DATA_ADDRESS(address)) {
        return false;
    }

    // Read an particular memory address
    data = *((uint8_t*)address);

    return true;
}

/**
  * @brief  Write a binary array to Flash
  * @param  data: pointer to data array
  * @param  size: array length
  * @param  address: Flash relative address to write (0 ... getSize())
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::write(uint32_t address, const uint8_t* data, uint32_t size)
{
    bool status = false;

    // Relative address -> to Absolute
    address += FLASH_BASE;

    // Check the Flash real addresss
    if (!IS_FLASH_DATA_ADDRESS(address)) {
        return false;
    }

    if (!IS_FLASH_DATA_ADDRESS(address + size)) {
        return false;
    }

    // Unlock Flash (enable Write accsess)
    if (HAL_FLASH_Unlock() == HAL_OK) {
		
        while(size--) {
            if (checkSector(address) == false) {
				status = false;
                break;
            }
			
            // Write byte at m_TmpValue
            if (tmpWrite(address, *data)) {
                // Program word at a specified address
                if (HAL_FLASH_Program(TYPEPROGRAM_WORD, address - 3, m_tmpValue) == HAL_OK)  {
                    status = true;
                } else {
                    status = false;
                    break;
                }
            }
			
            address++;
            data++;
        }
		
        // Lock FLASH (disable Write accsess)
        if (HAL_FLASH_Lock() != HAL_OK) {
            status = false;
        }
    }
	
    return status;
}

/**
  * @brief  Write a binary array to Flash
  * @param  data: byte which will be written to the memory
  * @param  size: array length
  * @param  address: Flash relative address to write (0 ... getSize())
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::write(uint32_t address, uint8_t data, uint32_t size)
{
    bool status = false;

    // Relative address -> to Absolute
    address += FLASH_BASE;

    // Check the Flash real addresss
    if (!IS_FLASH_DATA_ADDRESS(address)) {
        return false;
    }

	if (!IS_FLASH_DATA_ADDRESS(address + size)) {
        return false;
    }

    // Unlock Flash (enable Write accsess)
    if (HAL_FLASH_Unlock() == HAL_OK) {
        while(size--) {
            if (checkSector(address) == false) {
                status = false;
                break;
            }
			
            // Write byte at m_TmpValue
            if (tmpWrite(address, data)) {
                // Program word at a specified address
                if (HAL_FLASH_Program(TYPEPROGRAM_WORD, address - 3, m_tmpValue) == HAL_OK)  {
                    status = true;
                } else {
                    status = false;
                    break;
                }
            }
			
            address++;
        }
		
        // Lock FLASH (disable Write accsess)
        if (HAL_FLASH_Lock() != HAL_OK) {
            status = false;
        }
    }
	
    return status;
}


/**
  * @brief  Read a binary array from Flash
  * @param  address: Flash relative address to read
  * @param  data: outpu data array pointer
  * @param  size: array length
  * @retval true if OK,  otherwise return false
  */
bool Drivers::Flash::read(uint32_t address, uint8_t* data, uint32_t size)
{
    // Relative address -> to Absolute
    address += FLASH_BASE;

    // Check the EEPROM real addresss
    if (!IS_FLASH_DATA_ADDRESS(address)) {
        return false;
    }

    // Read data
    memcpy(data, (uint8_t*)(address), size);

    return true;
}
