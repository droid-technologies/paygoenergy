#include "Uart.h"

std::map<Drivers::Uart*, Drivers::Uart::UartNumber> Drivers::Uart::m_objects;

/**
  * @brief  The constructor UART init class
  * @param  uart_instance: UARTx instance (USART1 ... USART2)
  * @param  default_baudrate: baudrate (kUartBaudRate_9600 ... kUartBaudRate_115200)
  * @retval none
  */
Drivers::Uart::Uart(UartNumber port, UartBaudRate baud)
{
    RCC_PeriphCLKInitTypeDef PeriphClkInit;
    GPIO_InitTypeDef GPIO_InitStruct;

    // Get clock settings first
    HAL_RCCEx_GetPeriphCLKConfig(&PeriphClkInit);

    // Init UART Periph Clock
    if (port == kUartNumber1) {
        PeriphClkInit.PeriphClockSelection |= RCC_PERIPHCLK_USART1;
        PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_SYSCLK;
    } else {
        PeriphClkInit.PeriphClockSelection |= RCC_PERIPHCLK_USART2;
        PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_SYSCLK;
    }

    // Update new clock setiings
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
        return;
    }

    if (port == kUartNumber1) {
        // Peripheral clock enable
        __HAL_RCC_USART1_CLK_ENABLE();

        // USART1 GPIO Configuration
        // PB6      ------> USART1_TX
        // PB7     ------> USART1_RX
        GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

        // Peripheral interrupt init
        HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
    } else {
        // UART2

        // Peripheral clock enable
        __HAL_RCC_USART2_CLK_ENABLE();

        // USART2 GPIO Configuration
        // PA2     ------> USART2_TX
        // PA3     ------> USART2_RX
        GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF4_USART2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        // Peripheral interrupt init
        HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USART2_IRQn);
    }

    // Init Settings
    if (port == kUartNumber1) {
        m_hUARTx.Instance = USART1;
    } else {
        m_hUARTx.Instance = USART2;
    }
    m_hUARTx.Init.BaudRate = baud;
    m_hUARTx.Init.WordLength = UART_WORDLENGTH_8B;
    m_hUARTx.Init.StopBits = UART_STOPBITS_1;
    m_hUARTx.Init.Parity = UART_PARITY_NONE;
    m_hUARTx.Init.Mode = UART_MODE_TX_RX;
    m_hUARTx.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    m_hUARTx.Init.OverSampling = UART_OVERSAMPLING_16;
    m_hUARTx.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    m_hUARTx.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    m_hUARTx.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
    m_hUARTx.AdvancedInit.OverrunDisable = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT;

    // Write Settings
    if (HAL_UART_Init(&m_hUARTx) != HAL_OK) {
        return;
    }

    // Set this object and UART number
    m_objects.insert(std::pair<Uart*, UartNumber>(this, port));

    // Startup  variable init
    m_rxIndex = 0;
    m_numOfDataReceived = 0;
    // Start receiving procedure
    HAL_UART_Receive_IT(&m_hUARTx, &m_rxData, 1);
}


/**
  * @brief  Destructor
  * @note   not used yet
  * @retval none
  */
Drivers::Uart::~Uart()
{
/*
    if (m_hUARTx) {
        delete m_hUARTx;
    }
*/
}

/**
  * @brief  Send a data to UARTx
  * @param  data: pointer to data source array
  * @param  length: array length
  * @retval none
  */
void Drivers::Uart::sendData(uint8_t* data, uint16_t length)
{
    HAL_UART_Transmit_IT(&m_hUARTx, data, length);
}


/**
  * @brief  Receive a data from UARTx
  * @param  data: pointer to destination data array
  * @param  length: result array length
  * @param  maxSize: maximum number of bytes that data buffer can received
  * @retval none
  */
void Drivers::Uart::readData(uint8_t* data, uint16_t& length, uint16 maxSize)
{
    uint8_t size, i, j;

    length = 0;
    if (!m_rxIndex) {
        return;
    }

    // Set disable all interrupt
    DISABLE_INTERRUPT();

    // Check buff size
    size = m_rxIndex;
    if (size > maxSize) {
        size = maxSize;
    }

    // Copy existing data
    for (i = 0; i < size; i++) {
        data[i] = m_rxBuffer[i];
        m_rxIndex--;
    }
    //rxIndex -= size; // Check BUG!!!???

    // Shift remaining data
    j = 0;
    while (i < m_rxIndex) {
        m_rxBuffer[j++] = m_rxBuffer[i++];
    };

    // Set enable all interrupt
    ENABLE_INTERRUPT();

    // Number of data was recived
    length = size;
    m_numOfDataReceived += size;
}

/**
  * @brief  Change UART baudrate
  * @param  baud: new baudrate (kUartBaudRate_9600 ... kUartBaudRate_115200)
  * @retval none
  */
void Drivers::Uart::setBaudRate(UartBaudRate baud)
{
    // Update baudrate
    m_hUARTx.Init.BaudRate = baud;
    // Write Settings
    HAL_UART_Init(&m_hUARTx);

    // Clr Rx index
    m_rxIndex = 0;
    // Restart receiving procedure
    HAL_UART_Receive_IT(&m_hUARTx, &m_rxData, 1);
}

/**
  * @brief  Status of Transmitting
  * @param  none
  * @retval true - can start new Transmition,
  *         false - previos transmittion in progress (busy)
  */
bool Drivers::Uart::getStatusTx(void)
{
    if (m_hUARTx.gState == HAL_UART_STATE_READY) {
        return true;
    }
    return false;
}

/**
  * @brief  Add new byte from UART in to Receiver buffer
  * @param  none
  * @retval none
  * @notes  This function called from rxBufferAddCallback() static function
  */
void Drivers::Uart::rxBufferAdd(void)
{
    // Check buffer overflow
    if (m_rxIndex < kUartRxBufferSize) {
        m_rxBuffer[m_rxIndex++] = m_hUARTx.Instance->RDR;
    }

    // Clear the buffer to prevent overrun
    __HAL_UART_SEND_REQ(&m_hUARTx, UART_RXDATA_FLUSH_REQUEST);

    // Restart receiving procedure
    HAL_UART_Receive_IT(&m_hUARTx, &m_rxData, 1);
}

/**
  * @brief  Find UART handle from in number
  * @param  u_number: kUartNumber1...kUartNumber2...
  * @retval UART handle
  * @notes  This function must be called from USARTx_IRQHandler() ISR
  */
UART_HandleTypeDef* Drivers::Uart::getHandle(UartNumber u_number)
{
    for (std::map<Uart*, UartNumber>::iterator it = m_objects.begin(); it != m_objects.end(); ++it) {
        if (it->second == u_number) {
            return &it->first->m_hUARTx;
        }
    }

    return NULL;
};

/**
  * @brief  Find coresponding UART input buffer from handle
  * @param  huart: handle
  * @retval none
  * @notes  This function must be called from HAL_UART_RxCpltCallback() function
  */
void Drivers::Uart::rxBufferAddCallback(UART_HandleTypeDef* huart)
{
    for (std::map<Uart*, UartNumber>::iterator it = m_objects.begin(); it != m_objects.end(); ++it) {
        if (&it->first->m_hUARTx == huart) {
            return it->first->rxBufferAdd();
        }
    }
}

// Interrupts functions
extern "C" void USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(Drivers::Uart::getHandle(Drivers::Uart::kUartNumber1));
}
extern "C" void USART2_IRQHandler(void)
{
    HAL_UART_IRQHandler(Drivers::Uart::getHandle(Drivers::Uart::kUartNumber2));
}

extern "C" void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    Drivers::Uart::rxBufferAddCallback(huart);
}