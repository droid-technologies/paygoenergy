#ifndef __PORT_H
#define __PORT_H

#if   defined(STM32L071xx)
    #include "stm32l0xx_hal.h"
    #include "Typedefs.h"
    #define DISABLE_INTERRUPT()       __disable_interrupt() 
    #define ENABLE_INTERRUPT()        __enable_interrupt()   
#else
    #error   "NOT SUPPORT CORE!!!!!!!!!!!!!"
#endif

#endif // __LEVAUX_PORT_H

