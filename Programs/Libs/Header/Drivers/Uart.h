#ifndef __LEVAUX_UART_H
#define __LEVAUX_UART_H

#include <map>
#include "Porting.h"

namespace Drivers
{
    /**
    * @brief      Class operate with STM32L0 UART module throw HAL functions
    * @note       None
    * @bug        None
    */

    class Uart
    {
    public:
        enum UartNumber
        {
            kUartNumber1 = USART1_BASE,
            kUartNumber2 = USART2_BASE
        };

        enum UartBaudRate
        {
            kUartBaudRate9600   = 9600,
            kUartBaudRate19200  = 19200,
            kUartBaudRate38400  = 38400,
            kUartBaudRate57600  = 57600,
            kUartBaudRate115200 = 115200,
            kUartBaudRate128000 = 128000,
            kUartBaudRate256000 = 256000
        };
        Uart(UartNumber port = kUartNumber1, UartBaudRate baud = kUartBaudRate115200);
        virtual ~Uart();
        void sendData(uint8_t* data, uint16_t length);
        void readData(uint8_t* data, uint16_t& length, uint16 maxSize);
        void setBaudRate(UartBaudRate baud = kUartBaudRate57600);
        bool getStatusTx(void);
        //
        int32_t getCounterRx(void) { return m_numOfDataReceived; };
        void resetCounterRx(void) { m_numOfDataReceived = 0; };
        void rxBufferAdd(void);
        //
        static UART_HandleTypeDef* getHandle(UartNumber u_number);
        static void rxBufferAddCallback(UART_HandleTypeDef* huart);

    private:
        // Receiver buffer size
        static const uint8_t kUartRxBufferSize =  200;
        //
        UART_HandleTypeDef m_hUARTx;
        uint8_t m_rxBuffer[kUartRxBufferSize];
        uint8_t m_rxData;
        uint8_t m_rxIndex;
        uint32_t m_numOfDataReceived;
        //
        static std::map<Uart*, UartNumber> m_objects;
    };
} // namespace Drivers

#endif // __UART_H