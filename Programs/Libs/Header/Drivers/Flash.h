#ifndef __DRIVERS_FLASH_H
#define __DRIVERS_FLASH_H

#include "Porting.h"
#include <string.h>

namespace Drivers
{
    /**
      * @brief      Class operate with STM32L0 FLASH throw HAL functions
      * @note       None
      * @bug        None
      */
    class Flash
    {
    public:
        bool write(uint32_t address, uint8_t data);
        bool read(uint32_t address, uint8_t& data);
        bool write(uint32_t address, const uint8_t* data, uint32_t size);
        bool write(uint32_t address, uint8_t data, uint32_t size);
        bool read(uint32_t address, uint8_t* data, uint32_t size);
		
	private:
		bool tmpWrite(uint32_t address, uint8_t value);
		bool checkSector(uint32_t address);
		uint32_t m_tmpValue;
    };
} // namespace Drivers

#endif // __DRIVERS_FLASH_H