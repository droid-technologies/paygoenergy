#ifndef _TYPEDEF_
#define _TYPEDEF_
 

 
#ifndef LINUX
typedef unsigned long long uint64_t;
typedef signed long long int64_t;
typedef unsigned int uint32_t;
typedef signed int int32_t;
typedef unsigned short uint16_t;
typedef signed short int16_t;
typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef char char8_t;
 
typedef unsigned long long uint64;
typedef signed long long int64;


//typedef unsigned int uint32;
 
typedef signed int int32;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned char uint8;

//typedef signed char int8;
 
typedef char char8;
typedef float float_t;
typedef double double_t;
#endif

typedef int BOOL;
//enum {FALSE = 0, TRUE = !FALSE};
#define FALSE 0
#define TRUE 1


#endif /* defined(_TYPEDEF_) */

 